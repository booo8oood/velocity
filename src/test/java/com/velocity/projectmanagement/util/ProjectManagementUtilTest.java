package com.velocity.projectmanagement.util;

import java.util.Map;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ProjectManagementUtilTest
{
    @Test
    public void testGetJSON()
    {
        String body = "{"
                + "\"id\" : 1,"
                + "\"code\" : \"TEST\","
                + "\"name\" : \"Test Project\","
                + "\"description\" : \"Description\""
                + "}";
        Map<String, Object> jsonMap = ProjectManagementUtil.getJSON(body);
        assertEquals(1, jsonMap.get("id"));
        assertEquals("TEST", jsonMap.get("code"));
        assertEquals("Test Project", jsonMap.get("name"));
        assertEquals("Description", jsonMap.get("description"));
    }
}
