package com.velocity.projectmanagement.generator;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import com.velocity.projectmanagement.entity.Task;

public class SimpleScheduleGeneratorTest
{
    SimpleScheduleGenerator generator;

    @Before
    public void setup()
    {
        generator = new SimpleScheduleGenerator();
    }
 
    @Test
    public void testProjectManagementServiceSimpleTask()
    {
        Set<Task> tasks = new HashSet<Task>();
        Task task = new Task();
        task.setId(1);
        task.setProjectCode("TEST");
        task.setDescription("Task 1");
        task.setDuration(1);
        tasks.add(task);

        generator.init(new Date(), tasks);
        generator.sort();
        generator.calculate();
        generator.plot();
        generator.display(generator.plot());
    }

    @Test
    public void testProjectManagementServiceTaskWithDependency()
    {
        Set<Task> tasks = new HashSet<Task>();
        Task task1 = new Task();
        task1.setId(1);
        task1.setProjectCode("TEST");
        task1.setDescription("Task 1");
        task1.setDuration(1);
        tasks.add(task1);

        Task task2 = new Task();
        task2.setId(2);
        task2.setProjectCode("TEST");
        task2.setDescription("Task 2");
        task2.setDuration(2);
        
        task2.addDependency(1);
        tasks.add(task2);
        
        generator.init(new Date(), tasks);
        generator.sort();
        generator.calculate();
        generator.plot();
        generator.display(generator.plot());
    }

    @Test
    public void testProjectManagementServiceTaskWithMultipleDependency()
    {
        Set<Task> tasks = new HashSet<Task>();
        Task task1 = new Task();
        task1.setId(1);
        task1.setProjectCode("TEST");
        task1.setDescription("Task 1");
        task1.setDuration(1);
        tasks.add(task1);

        Task task2 = new Task();
        task2.setId(2);
        task2.setProjectCode("TEST");
        task2.setDescription("Task 2");
        task2.setDuration(2);

        task2.addDependency(1);
        tasks.add(task2);

        Task task3 = new Task();
        task3.setId(3);
        task3.setProjectCode("TEST");
        task3.setDescription("Task 3");
        task3.setDuration(3);
        tasks.add(task3);

        Task task4 = new Task();
        task4.setId(4);
        task4.setProjectCode("TEST");
        task4.setDescription("Task 4");
        task4.setDuration(4);
        task4.addDependency(1);
        task4.addDependency(2);
        task4.addDependency(3);
        tasks.add(task4);

        generator.init(new Date(), tasks);
        generator.sort();
        generator.calculate();
        generator.display(generator.plot());
    }
}
