package com.velocity.projectmanagement.service;

import java.util.Date;
import java.util.HashSet;

import org.assertj.core.util.DateUtil;
import org.junit.Before;
import org.junit.Test;

import com.velocity.projectmanagement.dto.ProjectDTO;
import com.velocity.projectmanagement.dto.TaskDTO;
import com.velocity.projectmanagement.entity.Task;

public class ProjectManagementServiceTest
{
    ProjectManagementService service;

    @Before
    public void setup()
    {
        service = new ProjectManagementService();
        ProjectDTO dto = new ProjectDTO();
        dto.setCode("TEST");
        dto.setDescription("Test Description");
        dto.setName("Test Project");

        service.createProject(dto);

        ProjectDTO dto1 = new ProjectDTO();
        dto1.setCode("TEST1");
        dto1.setDescription("Test1 Description");
        dto1.setName("Test1 Project");

        service.createProject(dto1);

    }
 
    @Test
    public void testProjectManagementServiceSimpleTask()
    {
        TaskDTO taskDto = new TaskDTO();
        taskDto.setProjectCode("TEST");
        taskDto.setDescription("Task 1");
        taskDto.setDuration("1");
        service.addTask(taskDto);
        service.generateSchedule(0, "TEST", new Date());
    }

    @Test
    public void testProjectManagementServiceTaskWithDependency()
    {
        TaskDTO taskDto1 = new TaskDTO();
        taskDto1.setProjectCode("TEST");
        taskDto1.setDescription("Task 1");
        taskDto1.setDuration("1");
        service.addTask(taskDto1);

        TaskDTO taskDto2 = new TaskDTO();
        taskDto2.setProjectCode("TEST");
        taskDto2.setDescription("Task 2");
        taskDto2.setDuration("2");
        
        taskDto2.addDependency(1);
        service.addTask(taskDto2);
        
        service.generateSchedule(0, "TEST", new Date());
    }

    @Test
    public void testProjectManagementServiceTaskWithMultipleDependency()
    {
        System.out.println("Simple Generator");
        service.setTasks(new HashSet<Task>());

        TaskDTO taskDto1 = new TaskDTO();
        taskDto1.setProjectCode("TEST");
        taskDto1.setDescription("Task 1");
        taskDto1.setDuration("7");
        service.addTask(taskDto1);

        TaskDTO taskDto2 = new TaskDTO();
        taskDto2.setProjectCode("TEST");
        taskDto2.setDescription("Task 2");
        taskDto2.setDuration("3");
        taskDto2.addDependency(1);
        service.addTask(taskDto2);
        
        TaskDTO taskDto3 = new TaskDTO();
        taskDto3.setProjectCode("TEST");
        taskDto3.setDescription("Task 3");
        taskDto3.setDuration("4");
        service.addTask(taskDto3);

        TaskDTO taskDto4 = new TaskDTO();
        taskDto4.setProjectCode("TEST");
        taskDto4.setDescription("Task 4");
        taskDto4.setDuration("5");
        taskDto4.addDependency(2);
        taskDto4.addDependency(3);
        service.addTask(taskDto4);

        TaskDTO taskDto5 = new TaskDTO();
        taskDto5.setProjectCode("TEST");
        taskDto5.setDescription("Task 5");
        taskDto5.setDuration("2");
        service.addTask(taskDto5);

        TaskDTO taskDto6 = new TaskDTO();
        taskDto6.setProjectCode("TEST");
        taskDto6.setDescription("Task 6");
        taskDto6.setDuration("4");
        service.addTask(taskDto6);

        TaskDTO taskDto7 = new TaskDTO();
        taskDto7.setProjectCode("TEST");
        taskDto7.setDescription("Task 7");
        taskDto7.setDuration("4");
        service.addTask(taskDto7);

        TaskDTO test1taskDto1 = new TaskDTO();
        test1taskDto1.setProjectCode("TEST1");
        test1taskDto1.setDescription("TEST1 Task 1");
        test1taskDto1.setDuration("10");
        service.addTask(test1taskDto1);

        service.generateSchedule(0, "TEST", DateUtil.parse("2018-07-16"));
    }

    @Test
    public void testProjectManagementServiceLeastSortedTaskWithMultipleDependency()
    {
        System.out.println("Least Generator");
        TaskDTO taskDto1 = new TaskDTO();
        taskDto1.setProjectCode("TEST");
        taskDto1.setDescription("Task 1");
        taskDto1.setDuration("7");
        service.addTask(taskDto1);

        TaskDTO taskDto2 = new TaskDTO();
        taskDto2.setProjectCode("TEST");
        taskDto2.setDescription("Task 2");
        taskDto2.setDuration("3");
        taskDto2.addDependency(1);
        service.addTask(taskDto2);
        
        TaskDTO taskDto3 = new TaskDTO();
        taskDto3.setProjectCode("TEST");
        taskDto3.setDescription("Task 3");
        taskDto3.setDuration("4");
        service.addTask(taskDto3);

        TaskDTO taskDto4 = new TaskDTO();
        taskDto4.setProjectCode("TEST");
        taskDto4.setDescription("Task 4");
        taskDto4.setDuration("5");
        taskDto4.addDependency(2);
        taskDto4.addDependency(3);
        service.addTask(taskDto4);

        TaskDTO taskDto5 = new TaskDTO();
        taskDto5.setProjectCode("TEST");
        taskDto5.setDescription("Task 5");
        taskDto5.setDuration("2");
        service.addTask(taskDto5);

        TaskDTO taskDto6 = new TaskDTO();
        taskDto6.setProjectCode("TEST");
        taskDto6.setDescription("Task 6");
        taskDto6.setDuration("4");
        service.addTask(taskDto6);

        TaskDTO taskDto7 = new TaskDTO();
        taskDto7.setProjectCode("TEST");
        taskDto7.setDescription("Task 7");
        taskDto7.setDuration("4");
        service.addTask(taskDto7);
        
        TaskDTO test1taskDto1 = new TaskDTO();
        test1taskDto1.setProjectCode("TEST1");
        test1taskDto1.setDescription("TEST1 Task 1");
        test1taskDto1.setDuration("10");
        service.addTask(test1taskDto1);
        
        service.generateSchedule(1, "TEST", DateUtil.parse("2018-07-16"));
    }

    @Test
    public void testProjectManagementServiceMostSortedTaskWithMultipleDependency()
    {
        System.out.println("Most Generator");
        TaskDTO taskDto1 = new TaskDTO();
        taskDto1.setProjectCode("TEST");
        taskDto1.setDescription("Task 1");
        taskDto1.setDuration("7");
        service.addTask(taskDto1);

        TaskDTO taskDto2 = new TaskDTO();
        taskDto2.setProjectCode("TEST");
        taskDto2.setDescription("Task 2");
        taskDto2.setDuration("3");
        taskDto2.addDependency(1);
        service.addTask(taskDto2);
        
        TaskDTO taskDto3 = new TaskDTO();
        taskDto3.setProjectCode("TEST");
        taskDto3.setDescription("Task 3");
        taskDto3.setDuration("4");
        service.addTask(taskDto3);

        TaskDTO taskDto4 = new TaskDTO();
        taskDto4.setProjectCode("TEST");
        taskDto4.setDescription("Task 4");
        taskDto4.setDuration("5");
        taskDto4.addDependency(2);
        taskDto4.addDependency(3);
        service.addTask(taskDto4);

        TaskDTO taskDto5 = new TaskDTO();
        taskDto5.setProjectCode("TEST");
        taskDto5.setDescription("Task 5");
        taskDto5.setDuration("2");
        service.addTask(taskDto5);

        TaskDTO taskDto6 = new TaskDTO();
        taskDto6.setProjectCode("TEST");
        taskDto6.setDescription("Task 6");
        taskDto6.setDuration("4");
        service.addTask(taskDto6);

        TaskDTO taskDto7 = new TaskDTO();
        taskDto7.setProjectCode("TEST");
        taskDto7.setDescription("Task 7");
        taskDto7.setDuration("4");
        service.addTask(taskDto7);
 
        TaskDTO test1taskDto1 = new TaskDTO();
        test1taskDto1.setProjectCode("TEST1");
        test1taskDto1.setDescription("TEST1 Task 1");
        test1taskDto1.setDuration("10");
        service.addTask(test1taskDto1);

        service.generateSchedule(2, "TEST", DateUtil.parse("2018-07-16"));
    }
}
