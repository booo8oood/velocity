package com.velocity.projectmanagement.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception thrown if the data is not found
 * @author jfmariano
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class NoDataException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    /**
     * Constructor
     * @param exception
     */
    public NoDataException(String exception)
    {
        super(exception);
    }
}
