package com.velocity.projectmanagement.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception thrown if the data is invalid
 * @author jfmariano
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidDataException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    /**
     * Constructor
     * @param exception
     */
    public InvalidDataException(String exception)
    {
        super(exception);
    }
}
