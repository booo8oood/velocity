package com.velocity.projectmanagement.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.velocity.projectmanagement.builder.ProjectManagementBuilder;
import com.velocity.projectmanagement.dto.ProjectDTO;
import com.velocity.projectmanagement.dto.ScheduleDTO;
import com.velocity.projectmanagement.dto.TaskDTO;
import com.velocity.projectmanagement.exception.InvalidDataException;
import com.velocity.projectmanagement.exception.NoDataException;
import com.velocity.projectmanagement.service.ProjectManagementService;

/**
 * Controller class for Project Management
 * @author jfmariano
 */
@RestController
@RequestMapping(path = "/projectmanagement", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProjectManagementController
{
    ProjectManagementService service = new ProjectManagementService();

    /**
     * Create a project
     * @param dto The dto containing the project information
     * @param ucBuilder Uri builder
     * @return The response with the location of the record in the header
     */
    @ResponseBody
    @PostMapping(path = "/projects", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> createProject(@RequestBody ProjectDTO dto, UriComponentsBuilder ucBuilder)
            throws InvalidDataException
    {
        String projectCode = null;
        projectCode = service.createProject(dto);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("projects/" + projectCode).buildAndExpand().toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }

    /**
     * Get an existing project
     * @param projectCode
     * @return The response with the project details
     * @throws InvalidDataException The project code is not supplied
     * @throws NoDataException The project does not exist
     */
    @ResponseBody
    @GetMapping(path = "/projects/{projectCode}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ProjectDTO getProject(@PathVariable("projectCode") String projectCode) throws InvalidDataException,
            NoDataException
    {
        return service.getProjectByCode(projectCode);
    }

    /**
     * Creates the task
     * @param projectCode
     * @param dto
     * @param ucBuilder The URI Component builder
     * @return The response with the location of the task in the header
     * @throws InvalidDataException The request data is invalid 
     */
    @ResponseBody
    @PostMapping(path = "/projects/{projectCode}/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> createTask(@PathVariable("projectCode") String projectCode, @RequestBody TaskDTO dto,
            UriComponentsBuilder ucBuilder) throws InvalidDataException
    {
        long taskId = 0;
        taskId = service.addTask(dto);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("tasks/" + taskId).buildAndExpand().toUri());
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }

    /**
     * Retrieves the task byid
     * @param id
     * @return The task details
     * @throws NoDataException The task does not exist
     */
    @ResponseBody
    @GetMapping(path = "/tasks/{taskId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public TaskDTO getTask(@PathVariable("taskId") long id) throws NoDataException
    {
        return service.getTaskById(id);
    }

    /**
     * Generates the schedule using an option
     * @param projectCode
     * @param option
     * @param startDate
     * @return The generated schedule dto
     */
    @ResponseBody
    @GetMapping(path = "/project/{projectCode}/schedule", produces = MediaType.APPLICATION_JSON_VALUE)
    public ScheduleDTO generateSchedule(@PathVariable("projectCode") String projectCode,
            @RequestParam("option") int option, @RequestParam("startDate") String startDate)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        ProjectDTO projectDTO = getProject(projectCode);
        Date start;
        try
        {
            start = dateFormat.parse(startDate);
        }
        catch (ParseException e)
        {
            throw new InvalidDataException(String.format("StartDate %s is invalid.", startDate));
        }
        Set<TaskDTO> tasksDTO = service.generateSchedule(option, projectCode, start);
        return ProjectManagementBuilder.build(projectDTO, tasksDTO);
    }
}
