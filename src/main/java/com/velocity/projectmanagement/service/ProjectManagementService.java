package com.velocity.projectmanagement.service;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.velocity.projectmanagement.builder.ProjectManagementBuilder;
import com.velocity.projectmanagement.dto.ProjectDTO;
import com.velocity.projectmanagement.dto.TaskDTO;
import com.velocity.projectmanagement.entity.Project;
import com.velocity.projectmanagement.entity.Task;
import com.velocity.projectmanagement.exception.InvalidDataException;
import com.velocity.projectmanagement.exception.NoDataException;
import com.velocity.projectmanagement.generator.IScheduleGenerator;
import com.velocity.projectmanagement.generator.ScheduleGeneratorFactory;
import com.velocity.projectmanagement.util.ProjectManagementUtil;
import com.velocity.projectmanagement.validator.ProjectManagementValidator;

/**
 * ProjectManagementService
 * @author jfmariano
 */
public class ProjectManagementService
{
    private Set<Project> projects = new HashSet<Project>();
    private Set<Task> tasks = new HashSet<Task>();

    /**
     * Creates the project
     * @param dto The dto containing the project information
     * @return The project code
     * @throws InvalidDataException If the project code is null/empty or the project exists
     */
    public String createProject(ProjectDTO dto) throws InvalidDataException
    {
        Project entity = ProjectManagementBuilder.build(dto);
        ProjectManagementValidator.validateParameterIsNotNullOrEmpty(dto.getCode(), "Project code is null or empty.");
        long id = ProjectManagementUtil.getNextProjectId(projects, entity.getCode());

        if (id == -1)
        {
            throw new InvalidDataException(String.format("Project code %s exists.", entity.getCode()));
        }

        entity.setId(id);
        projects.add(entity);
        return entity.getCode();
    }

    /**
     * Retrieves the project
     * @param code The project code
     * @return The project dto
     * @throws InvalidDataException The project code is null/empty
     * @throws NoDataException The project does not exist
     */
    public ProjectDTO getProjectByCode(String code) throws InvalidDataException, NoDataException
    {
        ProjectManagementValidator.validateParameterIsNotNullOrEmpty(code, "Project code is null or empty.");
        Project project = null;
        for (Project item : projects)
        {
            if (item.getCode().equals(code))
            {
                project = item;
            }
        }

        if (project == null)
        {
            throw new NoDataException("Project does not exist.");
        }
        
        return ProjectManagementBuilder.build(project);
    }

    /**
     * Retrieves the task by id
     * @param id
     * @return The task by id
     * @throws NoDataException The task does not exist
     */
    public TaskDTO getTaskById(long id) throws NoDataException
    {
        Task task = null;
        for (Task item : this.tasks)
        {
            if (item.getId() == id)
            {
                task = item;
            }
        }

        if (task == null)
        {
            throw new NoDataException(String.format("Task %d does not exist.", id));
        }
        
        return ProjectManagementBuilder.build(task);
    }

    /**
     * Retrieves the tasks for a project
     * @param projectCode
     * @return The tasks for a project
     */
    public Set<Task> getTasksForProject(String projectCode)
    {
        Set<Task> tasks = new HashSet<Task>();
        for (Task item : this.tasks)
        {
            if (item.getProjectCode().equals(projectCode))
            {
                tasks.add(item);
            }
        }
        return tasks;
    }

    /**
     * Adds a task to a project
     * @param dto
     * @return The task id
     * @throws InvalidDataException The duration is invalid
     */
    public long addTask(TaskDTO dto) throws InvalidDataException
    {
        // Validation for project
        getProjectByCode(dto.getProjectCode());

        // Validation for task and dependencies
        if (Integer.parseInt(dto.getDuration()) < 0)
        {
            throw new InvalidDataException("Invalid duration.");
        }

        for (Long dependencyItemId : dto.getDependency())
        {
            getTaskById(dependencyItemId);
        }

        Task entity = ProjectManagementBuilder.build(dto);
        long id = ProjectManagementUtil.getNextTaskId(tasks);
        entity.setId(id);
        tasks.add(entity);
        return entity.getId();
    }

    /**
     * Generates the schedule based on the generation option and startDAte
     * @param option
     * @param projectCode
     * @param startDate
     * @return The generated schedule
     */
    public Set<TaskDTO> generateSchedule(int option, String projectCode, Date startDate)
    {
        Set<Task> tasks = getTasksForProject(projectCode);
        IScheduleGenerator generator = ScheduleGeneratorFactory.getGenerator(option);
        generator.init(startDate, tasks);
        generator.sort();
        generator.calculate();
        generator.display(generator.plot());
        Set<TaskDTO> taskDtoSet = new HashSet<TaskDTO>();
        for (Task task : tasks)
        {
            taskDtoSet.add(ProjectManagementBuilder.build(task));
        }
        return taskDtoSet;
    }

    /**
     * Retrieves the projects
     * @return The set of projects
     */
    protected Set<Project> getProjects()
    {
        return projects;
    }

    /**
     * Sets the projects
     * @param projects
     */
    protected void setProjects(Set<Project> projects)
    {
        this.projects = projects;
    }

    /**
     * Retrieves the tasks
     * @return
     */
    protected Set<Task> getTasks()
    {
        return tasks;
    }

    /**
     * Sets the tasks
     * @param tasks
     */
    protected void setTasks(Set<Task> tasks)
    {
        this.tasks = tasks;
    }
}
