package com.velocity.projectmanagement.generator;

import java.util.Date;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

import com.velocity.projectmanagement.entity.Task;

/**
 * Abstract class for the ScheduleGenerator
 * @author jfmariano
 */
public abstract class AbstractScheduleGenerator implements IScheduleGenerator
{
    Queue<Task> sortedQueue = new LinkedList<Task>();

    /**
     * Initializes the generator
     * @param startDate The startDate
     * @param tasks The set of tasks to be scheduled
     */
    public abstract void init(Date startDate, Set<Task> tasks);

    /**
     * Sorts the tasks based on duration
     */
    public abstract void sort();

    /**
     * Calculates the total duration from start date
     */
    public abstract void calculate();

    /**
     * Plots the task with the scheduled dates
     * @return Array of tasks
     */
    public abstract Task[] plot();

    /**
     * Prints the tasks for debugging
     */
    public void display(Task[] tasks)
    {
        System.out.println("SCHEDULE");
        for (Task task : tasks)
        {
            System.out.println(task.toString());
        }
    }
}
