package com.velocity.projectmanagement.generator;

import java.util.Date;
import java.util.Set;

import com.velocity.projectmanagement.entity.Task;

/**
 * Interface for schedule generator
 * @author jfmariano
 */
public interface IScheduleGenerator
{
    /**
     * Initialize the generator
     * @param startDate
     * @param tasks
     */
    void init(Date startDate, Set<Task> tasks);

    /**
     * Sorts the task in the sorted queue
     */
    void sort();

    /**
     * Calculate the total duration
     */
    void calculate();

    /**
     * Plots the task to the dates
     * @return The tasks with dates
     */
    Task[] plot();

    /**
     * Prints the task for debugging
     * @param tasks
     */
    void display(Task[] tasks);
}
