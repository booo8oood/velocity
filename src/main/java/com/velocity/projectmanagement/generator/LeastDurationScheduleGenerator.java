package com.velocity.projectmanagement.generator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.velocity.projectmanagement.entity.Task;

/**
 * LeastDurationScheduleGenerator
 * Generates the tasks with priority to the least duration
 * @author jfmariano
 */
public class LeastDurationScheduleGenerator extends SimpleScheduleGenerator
{
    /**
     * Initialize the generator by sorting the tasks with the least duration
     */
    @Override
    public void init(Date startDate, Set<Task> tasks)
    {
        List<Task> tasksList = new ArrayList<Task>(tasks);
        Comparator<Task> listComparator = (Task o1, Task o2) -> (Long.valueOf(o1.getId()).compareTo(Long.valueOf(o2.getId())));
        Collections.sort(tasksList, listComparator);

        super.startDate = startDate;
        Comparator<Task> comparator = (Task o1, Task o2) -> (Integer.valueOf(o1.getDuration()).compareTo(Integer.valueOf(o2.getDuration())));
        Collections.sort(tasksList, comparator);

        super.unsortedQueue = new LinkedList<Task>();
        super.unsortedQueue.addAll(tasksList);
    }
}
