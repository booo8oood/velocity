package com.velocity.projectmanagement.generator;

/**
 * Factory for the schedule generator
 * @author jfmariano
 */
public class ScheduleGeneratorFactory
{
    /**
     * Retrieves the generator based on the option
     * @param option
     * @return The schedule generator
     */
    public static IScheduleGenerator getGenerator(int option)
    {
        IScheduleGenerator generator = null;
        switch(option)
        {
            case 0:
                generator = new SimpleScheduleGenerator();
                break;
            case 1:
                generator = new LeastDurationScheduleGenerator();
                break;
            case 2:
                generator = new MostDurationScheduleGenerator();
                break;
            default:
                generator = new SimpleScheduleGenerator();
                break;
        }
        return generator;
    }
}
