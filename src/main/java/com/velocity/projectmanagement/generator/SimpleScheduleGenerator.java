package com.velocity.projectmanagement.generator;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import com.velocity.projectmanagement.entity.Task;
import com.velocity.projectmanagement.util.ProjectManagementUtil;

/**
 * SimpleScheduleGenerator
 * @author jfmariano
 */
public class SimpleScheduleGenerator extends AbstractScheduleGenerator
{
    Queue<Task> unsortedQueue = null;
    Date startDate = null;

    /**
     * Initialize the generator by sorting the tasks with the natural order
     */
    @Override
    public void init(Date startDate, Set<Task> tasks)
    {
        List<Task> tasksList = new ArrayList<Task>(tasks);
        Comparator<Task> comparator = (Task o1, Task o2) -> (Long.valueOf(o1.getId()).compareTo(Long.valueOf(o2.getId())));
        Collections.sort(tasksList, comparator);

        this.startDate = startDate;
        unsortedQueue = new LinkedList<Task>();
        for (Task task : tasksList)
        {
            unsortedQueue.add(task);
        }
    }

    /**
     * Sorts the tasks into a sorted queue
     */
    @Override
    public void sort()
    {
        while (!unsortedQueue.isEmpty())
        {
            Task currentTask = unsortedQueue.poll();
            if (currentTask.getDependency().size() == 0)
            {
                sortedQueue.add(currentTask);
            }
            else
            {
                boolean isReady = true;
                for (Long dependency : currentTask.getDependency())
                {
                    if (!ProjectManagementUtil.contains(sortedQueue, dependency))
                    {
                        isReady = false;
                    }
                }
                if (isReady)
                {
                    sortedQueue.add(currentTask);
                }
                else
                {
                    unsortedQueue.add(currentTask);
                }
            }
        }
    }

    /**
     * Calculates the total duration
     */
    @Override
    public void calculate()
    {
        int lastTaskDuration = 0;
        int totalDuration = 0;
        for (Task task : sortedQueue)
        {
            totalDuration += lastTaskDuration;
            task.setTotalDuration(totalDuration);
            lastTaskDuration = task.getDuration();
        }
    }

    /**
     * Plots the tasks to dates
     * @return The array of tasks
     */
    @Override
    public Task[] plot()
    {
        LocalDateTime localDateTime = startDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        for (Task task : sortedQueue)
        {
            task.setStartDate(Date.from(localDateTime.plusDays(task.getTotalDuration()).atZone(ZoneId.systemDefault())
                    .toInstant()));
            task.setEndDate(Date.from(localDateTime.plusDays(task.getTotalDuration()).plusDays(task.getDuration() - 1)
                    .atZone(ZoneId.systemDefault()).toInstant()));
        }
        return (Task[]) sortedQueue.toArray(new Task[sortedQueue.size()]);
    }
}
