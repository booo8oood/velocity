package com.velocity.projectmanagement.validator;

import com.velocity.projectmanagement.exception.InvalidDataException;

/**
 * Validator class for fields
 * @author jfmariano
 */
public class ProjectManagementValidator
{
    /**
     * Private constructor
     */
    private ProjectManagementValidator()
    {
        
    }

    /**
     * Validate the parameter if not null
     * @param parameter
     * @param errorMessage
     * @throws InvalidDataException
     */
    public static void validateParameterIsNotNull(Object parameter, String errorMessage) throws InvalidDataException
    {
        if (parameter == null)
        {
            throw new InvalidDataException(errorMessage);
        }
    }

    /**
     * Validate the parameter if not null or empty
     * @param parameter
     * @param errorMessage
     * @throws InvalidDataException
     */
    public static void validateParameterIsNotNullOrEmpty(String parameter, String errorMessage) throws InvalidDataException
    {
        if (parameter == null || parameter.length() == 0)
        {
            throw new InvalidDataException(errorMessage);
        }
    }
}
