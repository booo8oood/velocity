package com.velocity.projectmanagement.builder;

import java.text.SimpleDateFormat;
import java.util.Set;

import com.velocity.projectmanagement.dto.ProjectDTO;
import com.velocity.projectmanagement.dto.ScheduleDTO;
import com.velocity.projectmanagement.dto.TaskDTO;
import com.velocity.projectmanagement.entity.Project;
import com.velocity.projectmanagement.entity.Task;

/**
 * Builder class
 * @author jfmariano
 */
public class ProjectManagementBuilder
{
    /**
     * Private constructor
     */
    private ProjectManagementBuilder()
    {
        
    }

    /**
     * Builds the project dto to entity
     * @param dto
     * @return The project entity
     */
    public static Project build(ProjectDTO dto)
    {
        Project entity = new Project();
        entity.setId(dto.getId());
        entity.setCode(dto.getCode());
        entity.setName(dto.getName());
        entity.setDescription(dto.getDescription());
        return entity;
    }

    /**
     * Builds the task dto to entity
     * @param dto
     * @return The task entity
     */
    public static Task build(TaskDTO dto)
    {
        Task entity = new Task();
        entity.setId(dto.getId());
        entity.setProjectCode(dto.getProjectCode());
        entity.setDescription(dto.getDescription());
        entity.setDependency(dto.getDependency());
        entity.setDuration(Integer.parseInt(dto.getDuration()));
        return entity;
    }

    /**
     * Builds the project entity to dto
     * @param entity
     * @return The project dto
     */
    public static ProjectDTO build(Project entity)
    {
        ProjectDTO dto = new ProjectDTO();
        dto.setId(entity.getId());
        dto.setCode(entity.getCode());
        dto.setName(entity.getName());
        dto.setDescription(entity.getDescription());
        return dto;
    }

    /**
     * Builds the task entity to dto
     * @param entity
     * @return The task dto
     */
    public static TaskDTO build(Task entity)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        TaskDTO dto = new TaskDTO();
        dto.setId(entity.getId());
        dto.setProjectCode(entity.getProjectCode());
        dto.setDescription(entity.getDescription());
        dto.setDuration(Integer.toString(entity.getDuration()));
        dto.setDependency(entity.getDependency());
        if (entity.getStartDate() != null)
        {
            dto.setStartDate(dateFormat.format(entity.getStartDate()));
        }

        if (entity.getEndDate() != null)
        {

            dto.setEndDate(dateFormat.format(entity.getEndDate()));
        }
        return dto;
    }

    /**
     * Builds the schedule dto from project and tasks DTOs
     * @param project
     * @param tasks
     * @return The schedule dto
     */
    public static ScheduleDTO build(ProjectDTO project, Set<TaskDTO> tasks)
    {
        ScheduleDTO dto = new ScheduleDTO();
        dto.setProject(project);
        dto.setTasks(tasks);
        return dto;
    }
}
