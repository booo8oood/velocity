package com.velocity.projectmanagement.util;

import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import org.springframework.boot.json.JsonParserFactory;

import com.velocity.projectmanagement.entity.Project;
import com.velocity.projectmanagement.entity.Task;

public class ProjectManagementUtil
{
    public static long getNextProjectId(Set<Project> project, String code)
    {
        boolean isUnique = true;
        long id = 0;

        for (Project item: project)
        {
            if (item.getId() > id)
            {
                id = item.getId();
            }
            if (item.getCode().equals(code))
            {
                isUnique = false;
            }
        }

        if (isUnique)
        {
            id += 1;
        }
        else
        {
            id = -1;
        }

        return id;
    }

    public static long getNextTaskId(Set<Task> task)
    {
        long id = 0;
        for (Task item: task)
        {
            if (item.getId() > id)
            {
                id = item.getId();
            }
        }
        return id + 1;
    }

    public static boolean contains(Queue<Task> queue, long id)
    {
        boolean result = false;
        for (Task queueItem : queue)
        {
            if (id == queueItem.getId())
            {
                result = true;
            }
        }
        return result;
    }

    public static Map<String, Object> getJSON(String body)
    {
        Map<String, Object> jsonMap = JsonParserFactory.getJsonParser().parseMap(body);
        System.out.println(jsonMap);
        return jsonMap;
    }
}
