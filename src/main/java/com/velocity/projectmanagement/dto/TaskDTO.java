package com.velocity.projectmanagement.dto;

import io.swagger.annotations.ApiModelProperty;

import java.util.HashSet;
import java.util.Set;

/**
 * TaskDTO
 * @author jfmariano
 */
public class TaskDTO
{
    private long id;
    private String projectCode;
    private String description;
    private String duration;
    private String startDate;
    private String endDate;
    private Set<Long> dependency = new HashSet<Long>();

    /**
     * Retrieves the id
     * @return The id
     */
    @ApiModelProperty(hidden=true)
    public long getId()
    {
        return id;
    }

    /**
     * Sets the id
     * @param id
     */
    public void setId(long id)
    {
        this.id = id;
    }

    /**
     * Retrieves the project code
     * @return The project code
     */
    @ApiModelProperty(hidden=true)
    public String getProjectCode()
    {
        return projectCode;
    }

    /**
     * Sets the project code
     * @param projectCode
     */
    public void setProjectCode(String projectCode)
    {
        this.projectCode = projectCode;
    }

    /**
     * Retrieves the description
     * @return The description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * Sets the description
     * @param description
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * Retrieves the duration of this task
     * @return The duration of this task
     */
    public String getDuration()
    {
        return duration;
    }

    /**
     * Sets the duration of this task
     * @param duration
     */
    public void setDuration(String duration)
    {
        this.duration = duration;
    }

    /**
     * Retrieves the task dependencies
     * @return The task dependencies
     */
    public Set<Long> getDependency()
    {
        return dependency;
    }

    /**
     * Sets the task dependencies
     * @param dependency
     */
    public void setDependency(Set<Long> dependency)
    {
        this.dependency = dependency;
    }

    /**
     * Adds task dependency
     * @param id
     */
    public void addDependency(long id)
    {
        this.dependency.add(id);
    }

    /**
     * Retrieves the start date
     * @return The start date
     */
    @ApiModelProperty(hidden=true)
    public String getStartDate()
    {
        return startDate;
    }

    /**
     * Sets the start date
     * @param startDate
     */
    public void setStartDate(String startDate)
    {
        this.startDate = startDate;
    }

    /**
     * Retrieves the end date
     * @return The end date
     */
    @ApiModelProperty(hidden=true)
    public String getEndDate()
    {
        return endDate;
    }

    /**
     * Sets the end date
     * @param endDate
     */
    public void setEndDate(String endDate)
    {
        this.endDate = endDate;
    }
}
