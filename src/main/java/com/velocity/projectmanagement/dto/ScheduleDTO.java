package com.velocity.projectmanagement.dto;

import java.util.Set;

/**
 * The ScheduleDTO
 * @author jfmariano
 */
public class ScheduleDTO
{
    private ProjectDTO project;
    private Set<TaskDTO> tasks;

    /**
     * Retrieves the project
     * @return
     */
    public ProjectDTO getProject()
    {
        return project;
    }

    /**
     * Sets the project
     * @param project
     */
    public void setProject(ProjectDTO project)
    {
        this.project = project;
    }

    /**
     * Retrieves the tasks
     * @return The tasks
     */
    public Set<TaskDTO> getTasks()
    {
        return tasks;
    }

    /**
     * Sets the tasks
     * @param tasks
     */
    public void setTasks(Set<TaskDTO> tasks)
    {
        this.tasks = tasks;
    }
}
