package com.velocity.projectmanagement.dto;

import io.swagger.annotations.ApiModelProperty;

/**
 * ProjectDTO
 * @author jfmariano
 */
public class ProjectDTO
{
    private long id;
    private String code;
    private String name;
    private String description;

    /**
     * Retrieves the id
     * @return The id of this project
     */
    @ApiModelProperty(hidden=true)
    public long getId()
    {
        return id;
    }

    /**
     * Sets the id
     * @param id
     */
    public void setId(long id)
    {
        this.id = id;
    }

    /**
     * Retrieves the project code
     * @return The project code
     */
    public String getCode()
    {
        return code;
    }

    /**
     * Sets the project code
     * @param code
     */
    public void setCode(String code)
    {
        this.code = code;
    }

    /**
     * Retrieves the name
     * @return The name of the project
     */
    public String getName()
    {
        return name;
    }

    /**
     * Sets the name of the project
     * @param name
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Retrieves the description
     * @return The description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * Sets the description
     * @param description
     */
    public void setDescription(String description)
    {
        this.description = description;
    }
}
