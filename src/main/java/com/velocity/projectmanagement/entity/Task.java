package com.velocity.projectmanagement.entity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Task entity
 * @author jfmariano
 */
public class Task
{
    private long id;
    private String projectCode;
    private String description;
    private int duration;
    private int totalDuration;
    private Set<Long> dependency = new HashSet<Long>();
    private Date startDate;
    private Date endDate;

    /**
     * Retrieves the task id
     * @return the task id
     */
    public long getId()
    {
        return id;
    }

    /**
     * Sets the task id
     * @param id
     */
    public void setId(long id)
    {
        this.id = id;
    }

    /**
     * Retrieves the project code this task belongs to
     * @return The project code
     */
    public String getProjectCode()
    {
        return projectCode;
    }

    /**
     * Sets the project code
     * @param projectCode
     */
    public void setProjectCode(String projectCode)
    {
        this.projectCode = projectCode;
    }

    /**
     * Retrieves the description of the project
     * @return The description of the project
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * Sets the description of the project
     * @param description
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * Retrieves the duration set in days
     * @return The duration set
     */
    public int getDuration()
    {
        return duration;
    }

    /**
     * Sets the duration in days
     * @param duration
     */
    public void setDuration(int duration)
    {
        this.duration = duration;
    }

    /**
     * Retrieves the totalDuration from the startDate
     * @return The totalDuration form the startDate
     */
    public int getTotalDuration()
    {
        return totalDuration;
    }

    /**
     * Sets the totalDuration from the startDate
     * @param totalDuration
     */
    public void setTotalDuration(int totalDuration)
    {
        this.totalDuration = totalDuration;
    }

    /**
     * Retrieves the IDs of the task dependency
     * @return The set of IDs this task is dependent to
     */
    public Set<Long> getDependency()
    {
        return dependency;
    }

    /**
     * Sets the IDs of the task dependency
     * @param dependency
     */
    public void setDependency(Set<Long> dependency)
    {
        this.dependency = dependency;
    }

    /**
     * Retrieve the generated endDate
     * @return The endDate
     */
    public Date getEndDate()
    {
        return endDate;
    }

    /**
     * Sets the generated endDate
     * @param endDate
     */
    public void setEndDate(Date endDate)
    {
        this.endDate = endDate;
    }

    /**
     * Retrieves the generated startDate
     * @return The startDate
     */
    public Date getStartDate()
    {
        return startDate;
    }

    /**
     * Sets the generated startDate
     * @param startDate
     */
    public void setStartDate(Date startDate)
    {
        this.startDate = startDate;
    }

    /**
     * Add an ID as dependency
     * @param id
     */
    public void addDependency(long id)
    {
        this.dependency.add(id);
    }

    /**
     * The string format of this Task
     */
    public String toString()
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return id + " " + description + "[" + dependency.toString() + "]" + " Duration:" + duration + " Start Date: "
                + (startDate != null ? dateFormat.format(startDate) : "") + " End Date: "
                + (endDate != null ? dateFormat.format(endDate) : "");
    }
}
