package com.velocity.projectmanagement.entity;

/**
 * Project entity
 * @author jfmariano
 */
public class Project
{
    private long id;
    private String code;
    private String name;
    private String description;

    /**
     * Retrieve the id
     * @return the id
     */
    public long getId()
    {
        return id;
    }

    /**
     * Sets the id
     * @param id
     */
    public void setId(long id)
    {
        this.id = id;
    }

    /**
     * Retrieves the code
     * @return the code
     */
    public String getCode()
    {
        return code;
    }

    /**
     * Sets the code
     * @param code
     */
    public void setCode(String code)
    {
        this.code = code;
    }

    /**
     * Retrieves the project name
     * @return the project name
     */
    public String getName()
    {
        return name;
    }

    /**
     * Sets the project name
     * @param name
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Retrieves the description
     * @return the description
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * Sets the description
     * @param description
     */
    public void setDescription(String description)
    {
        this.description = description;
    }

    /**
     * Converts the entity to string
     */
    public String toString()
    {
        return id + " " + code;
    }
}
