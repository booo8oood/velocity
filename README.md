Build: mvn -U package eclipse:clean eclipse:eclipse<br>
Run: mvn spring-boot:run<br>

You can access the controllers using swagger ui for creating projects and tasks.<br>
http://localhost:8080/swagger-ui.html#/project-management-controller<br>

For ease of use I provided the postman run I've used in implementing:<br>
https://www.getpostman.com/collections/afac870709173493aaf4<br>

For projects:<br>
<b>POST</b> /projectmanagement/projects<br>
{<br>
    "code" : "TEST",<br>
    "name" : "Test Project",<br>
    "description" : "Test Project Description"<br>
}<br>

For tasks:<br>
<b>POST</b> /projectmanagement/projects/{projectCode}/tasks<br>
{<br>
    "projectCode" : "TEST",<br>
    "description" : "Task 4",<br>
    "duration" : 5,<br>
    "dependency" : [2, 3]<br>
}<br>

For generating the schedule:<br>
<b>GET</b> /projectmanagement/project/TEST/schedule?option=2&startDate=2018-07-16<br>

I provided three options to generate the schedule based on the duration:<br>
Option 0: <b>Simple Generator</b><br>
<table>
<tr>
  <th>Task</th>
  <th>Duration</th>
  <th>Dependency</th>
  <th>Start Date</th>
  <th>End Date</th>
</tr>
<tr>
  <td>Task 1</td>
  <td>7</td>
  <td></td>
  <td>2018-07-16</td>
  <td>2018-07-22</td>
</tr>
<tr>
  <td>Task 2</td>
  <td>3</td>
  <td>1</td>
  <td>2018-07-23</td>
  <td>2018-07-25</td>
</tr>
<tr>
  <td>Task 3</td>
  <td>4</td>
  <td></td>
  <td>2018-07-26</td>
  <td>2018-07-29</td>
</tr>
<tr>
  <td>Task 4</td>
  <td>5</td>
  <td>2, 3</td>
  <td>2018-07-30</td>
  <td>2018-08-03</td>
</tr>
<tr>
  <td>Task 5</td>
  <td>2</td>
  <td></td>
  <td>2018-08-04</td>
  <td>2018-08-05</td>
</tr>
<tr>
  <td>Task 6</td>
  <td>4</td>
  <td></td>
  <td>2018-08-06</td>
  <td>2018-08-09</td>
</tr>
<tr>
  <td>Task 7</td>
  <td>4</td>
  <td></td>
  <td>2018-08-10</td>
  <td>2018-08-13</td>
</tr>
</table>

Option 1: <b>Least Generator</b><br>
<table>
<tr>
  <th>Task</th>
  <th>Duration</th>
  <th>Dependency</th>
  <th>Start Date</th>
  <th>End Date</th>
</tr>
<tr>
  <td>Task 5</td>
  <td>2</td>
  <td></td>
  <td>2018-07-16</td>
  <td>2018-07-17</td>
</tr>
<tr>
  <td>Task 3</td>
  <td>4</td>
  <td></td>
  <td>2018-07-18</td>
  <td>2018-07-21</td>
</tr>
<tr>
  <td>Task 6</td>
  <td>4</td>
  <td></td>
  <td>2018-07-22</td>
  <td>2018-07-25</td>
</tr>
<tr>
  <td>Task 7</td>
  <td>4</td>
  <td></td>
  <td>2018-07-26</td>
  <td>2018-07-29</td>
</tr>
<tr>
  <td>Task 1</td>
  <td>7</td>
  <td></td>
  <td>2018-07-30</td>
  <td>2018-08-05</td>
</tr>
<tr>
  <td>Task 2</td>
  <td>3</td>
  <td>1</td>
  <td>2018-08-06</td>
  <td>2018-08-08</td>
</tr>
<tr>
  <td>Task 4</td>
  <td>5</td>
  <td>2, 3</td>
  <td>2018-08-09</td>
  <td>2018-08-13</td>
</tr>
</table>

Option 2: <b>Most Generator</b><br>
<table>
<tr>
  <th>Task</th>
  <th>Duration</th>
  <th>Dependency</th>
  <th>Start Date</th>
  <th>End Date</th>
</tr>
<tr>
  <td>Task 1</td>
  <td>7</td>
  <td></td>
  <td>2018-07-16</td>
  <td>2018-07-22</td>
</tr>
<tr>
  <td>Task 3</td>
  <td>4</td>
  <td></td>
  <td>2018-07-23</td>
  <td>2018-07-26</td>
</tr>
<tr>
  <td>Task 6</td>
  <td>4</td>
  <td></td>
  <td>2018-07-27</td>
  <td>2018-07-30</td>
</tr>
<tr>
  <td>Task 7</td>
  <td>4</td>
  <td></td>
  <td>2018-07-31</td>
  <td>2018-08-03</td>
</tr>
<tr>
  <td>Task 2</td>
  <td>3</td>
  <td>1</td>
  <td>2018-08-04</td>
  <td>2018-08-06</td>
</tr>
<tr>
  <td>Task 5</td>
  <td>2</td>
  <td></td>
  <td>2018-08-07</td>
  <td>2018-08-08</td>
</tr>
<tr>
  <td>Task 4</td>
  <td>5</td>
  <td>2, 3</td>
  <td>2018-08-09</td>
  <td>2018-08-13</td>
</tr>
</table>